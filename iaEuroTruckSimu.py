from pyainl16 import *
from random import randint
from math import sqrt

class iaEuroTruckSimu:
    

    def start(self,gameParams):        
        self.actions = Actions()
        self.params = gameParams
        
    def moveCell(self, id):
        return
        
    def cutCell(self, id):
        return
        
    def createVirus(self, id):
        return
        
    def leaveCell(self, id):
        return
        
    def playTurn(self, m_cells, e_cells, virus, neutral):
        if len(m_cells) == 0:
            self.actions.add_surrender_action()
        else:
            #list_hostiles = detect_hostiles(m_cells, e_cells, virus)
            (list_cibles, plus_proche)   = self.detect_cibles(m_cells, e_cells)
            #list_neutral = detect_neutrals(m_cells, neutral)

            #self.farme(m_cells[0], neutral)
            self.attack(m_cells, plus_proche)

    def getAction(self):
        return self.actions

    def attack(self, m_cells, cible):
        self.go_towards(m_cells[0], cible.position.x, cible.position.y)

    def go_towards(self, m_cells, x, y):
        self.actions.add_move_action(m_cells.pcell_id, x, y)

    def detect_cibles(self, m_cells, e_cells, rayon = 10000):
        mposX = m_cells[0].position.x
        mposY = m_cells[0].position.y

        list_cibles = []
        plus_pres = 0
        dst_plus_pres = -1

        for e in e_cells:
            posX = e.position.x
            posY = e.position.y

            distance = sqrt(((posX - mposX)*(posX - mposX)) + ((posY - mposY)*(posY - mposY)))
            if(distance <= rayon):
                if(e.mass < m_cells[0].mass):
                    list_cibles.append((e, distance))
            if(distance < dst_plus_pres or dst_plus_pres == -1):
                        plus_pres = e

        return (list_cibles, plus_pres)
        
        
    def farme(self, cell, neutral):
        if(len(neutral) > 1000): #On itere de facon aleatoire
            proche = -1
            proche_dist = -1
            for x in range(1000):
                z = randint(0, len(neutral) - 1)
                dist = sqrt((neutral[z].position.x - cell.position.x) * (neutral[z].position.x - cell.position.x)
                            + (neutral[z].position.y - cell.position.y) * (neutral[z].position.y - cell.position.y))
                if dist < proche_dist or proche_dist == -1:
                    proche_dist = dist
                    proche = z

            print("move" + str(neutral[proche].position))
            self.actions.add_move_action(cell.pcell_id, neutral[proche].position.x + 100,  neutral[proche].position.y + 100)
        else: #On itere sur toute
            proche = -1
            proche_dist = -1
            for x in neutral:
                dist = sqrt((neutral[x].position.x - cell.position.x) * (neutral[x].position.x - cell.position.x)
                            + (neutral[x].position.y - cell.position.y) * (neutral[x].position.y - cell.position.y))
                if dist < proche_dist or proche_dist == -1:
                    proche_dist = dist
                    proche = x
            self.actions.add_move_action(cell.pcell_id, neutral[proche].position.x + 100, neutral[proche].position.y + 100)
