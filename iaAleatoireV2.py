from pyainl16 import *
from random import randint

class IaAleatoireV2:

    
    def start(self,gameParams):
        self.parameters = gameParams.parameters
        self.wel = gameParams
        self.actions = Actions()
        self.baseMass = ((self.parameters.player_cells_starting_mass * self.parameters.nb_starting_cells_per_player) * (3/4))
    def moveCell(self, cell):
        posx = randint(0,self.parameters.map_width)
        posy = randint(0,self.parameters.map_height)
        self.actions.add_move_action(cell.pcell_id, posx, posy)
        
    def cutCell(self, cell):
        posx = randint(0,self.parameters.map_width)
        posy = randint(0,self.parameters.map_height)
        mass = randint(int(cell.mass/4),int(cell.mass))
        self.actions.add_divide_action(cell.pcell_id,posx,posy,mass)

    def createVirus(self, cell):
        posx = randint(0,self.parameters.map_width)
        posy = randint(0,self.parameters.map_height)
        self.actions.add_move_action(cell.pcell_id,posx,posy)

    def surrender(self):
        self.actions.add_surrender_action()
        
    def playTurn(self, m_cells, e_cells, virus, neutral):
        self.actions.clear()
        sumMass = 0;
        for cell in m_cells:
            sumMass += cell.mass
        if(len(m_cells) > 0 and sumMass > self.baseMass):
            for cell in m_cells:
                res = randint(0,2)
                if(res == 0):
                    self.moveCell(cell)
                elif(res == 1 and len(m_cells) < self.parameters.max_cells_per_player):
                    self.cutCell(cell)
                else:
                    self.createVirus(cell)
        else:
            self.surrender()
                    

    def getAction(self):
        return self.actions
        
