from pyainl16 import *
from random import randint
from math import *
import operator

class IaAleatoireV2:
    
    def start(self,gameParams):
        self.parameters = gameParams.parameters
        self.wel = gameParams
        self.actions = Actions()
        self.baseMassDiv = ((self.parameters.player_cells_starting_mass * self.parameters.nb_starting_cells_per_player) * (4/5))
        self.baseMass = ((self.parameters.player_cells_starting_mass * self.parameters.nb_starting_cells_per_player))

    def moveCell(self, cell, targets, danger, virus):
            found = False
            nb = 0
            while not(found) and nb < 10:
                nb += 1
                if len(targets) > 0:
                    (dst, e) = targets[0]
                    found = True                    
                    for i in danger:
                        if (self.getDistance(e.position.x, e.position.y, i.position.x, i.position.y) < self.getDistance(cell.position.x, cell.position.y, i.position.x, i.position.y)) :
                            found = False
                        break
                    for i in virus:
                        if (self.getDistance(e.position.x, e.position.y, i.position.x, i.position.y) < self.getDistance(cell.position.x, cell.position.y, i.position.x, i.position.y)) :
                            found = False
                            break
                    
                    if found:                            
                        self.actions.add_move_action(cell.pcell_id, e.position.x, e.position.y)
                    else:
                        del targets[0]
              
            if nb >= 10:
                if(len(danger) == 0):
                    max_speed = max(0, self.parameters.base_cell_speed - cell.mass * self.parameters.speed_loss_factor)
                    rand = randint(-1, 1)
                    posx = randint(0,self.parameters.map_width) + (max_speed * rand)
                    posy = randint(0,self.parameters.map_height) + (max_speed * rand)
                    self.actions.add_move_action(cell.pcell_id, posx, posy)
                else:
                    min = -1
                    elem = None
                    for i in danger:
                        dist = self.getDistance(i.position.x, i.position.y, cell.position.x, cell.position.y)
                        if (dist < min or min == -1):
                            min = dist
                            elem = i
                    
                    max_speed = max(0, self.parameters.base_cell_speed - cell.mass * self.parameters.speed_loss_factor)
                    posx = cell.position.x - ((elem.position.x - cell.position.x) * max_speed)
                    posy = cell.position.y - ((elem.position.y - cell.position.y) * max_speed)
                   
                    self.actions.add_move_action(cell.pcell_id, posx, posy)
                        
                    print("fuite")
                    
                
        
    def cutCell(self, cell):
        posx = randint(0,self.parameters.map_width)
        posy = randint(0,self.parameters.map_height)
        self.actions.add_divide_action(cell.pcell_id,posx,posy,cell.mass//10)

    def createVirus(self, cell):
        posx = randint(0,self.parameters.map_width)
        posy = randint(0,self.parameters.map_height)
        self.actions.add_create_virus_action(cell.pcell_id, posx, posy)
        

    def surrender(self):
        self.actions.add_surrender_action()

    def getTargetsEnnemies(self, cell, e_cells):
        mposX = cell.position.x
        mposY = cell.position.y
        ray = 1500
        list_cibles = []
        list_danger = []
        plus_pres = (-1, 0)

        for e in e_cells:
            posX = e.position.x
            posY = e.position.y

            distance = sqrt(((posX - mposX)*(posX - mposX)) + ((posY - mposY)*(posY - mposY)))
            (dst, cible_plus_proche) = plus_pres
            if(distance <= ray):
                if(e.mass < cell.mass):
                    list_cibles.append((distance, e))
                else:
                    list_danger.append(e)
                    
            if(distance < dst or dst == -1):
                plus_pres = (distance, e)

        if len(e_cells) > 0:
            list_cibles.append(plus_pres)
        
        list_cibles.sort(key = operator.itemgetter(0))

        return (list_cibles, list_danger)

    def getDistance(self, xA, yA, xB, yB):
        return sqrt(((xB - xA)*(xB - xA)) + ((yB - yA)*(yB - yA)))
    
    def getPosNearBiggest(self, cell, puteCell):
        ray = cell.mass * self.parameters.radius_factor
        posxTop = cell.position.x
        posyTop = cell.position.y + ray

        posxBottom = cell.position.x
        posyBottom = cell.position.y - ray

        posxRight = cell.position.x + ray
        posyRight = cell.position.y

        posxLeft = cell.position.x - ray
        posyLeft = cell.position.y

        dst = [self.getDistance(puteCell.position.x, puteCell.position.y, posxTop, posyTop),
               self.getDistance(puteCell.position.x, puteCell.position.y, posxBottom, posyBottom),
               self.getDistance(puteCell.position.x, puteCell.position.y, posxRight, posyRight),
               self.getDistance(puteCell.position.x, puteCell.position.y, posxLeft, posyLeft)]

        min = -1
        index = -1
        for i,d in enumerate(dst):
            if d < min or min == -1:
                min = d
                index = i

        if index == 0:
            return (posxTop, posyTop)
        elif index == 1:
            return (posxBottom, posyBottom)
        elif index == 2:
            return (posxRight, posyRight)
        else:
            return (posxLeft, posyLeft)
            
    def playTurn(self, m_cells, e_cells, virus, neutral):
        self.actions.clear()
        sumMass = 0;
        biggest_mass = 0
        biggest_cell = None
        for cell in m_cells:
            sumMass += cell.mass
            if cell.mass > biggest_mass:
                biggest_mass = cell.mass
                biggest_cell = cell
        if(len(m_cells) > 0 and sumMass > self.baseMassDiv):
            for cell in m_cells:
                res = randint(0, 20)
                targets = None
                if(cell.mass > (1/2)*(self.baseMass)):
                    (targets, danger) = self.getTargetsEnnemies(cell, e_cells)
                    if(res == 1 and len(m_cells) < self.parameters.max_cells_per_player and cell.mass > (2/10)*(self.baseMass)):
                        self.cutCell(cell)
                    else:
                        self.moveCell(cell,targets, danger, virus)
                else:
                    (posx, posy) = self.getPosNearBiggest(biggest_cell, cell)
                    biggest = biggest_cell
                    biggest.position.x = posx
                    biggest.position.y = posy
                    self.moveCell(cell, [(0, biggest)], [], virus)
               
                    
        else:
            self.surrender()
                    

    def getAction(self):
        return self.actions
        
