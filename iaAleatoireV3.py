from pyainl16 import *
from random import randint
from math import *

class IaAleatoireV2:

    
    def start(self,gameParams):
        self.parameters = gameParams.parameters
        self.wel = gameParams
        self.actions = Actions()
        self.baseMass = ((self.parameters.player_cells_starting_mass * self.parameters.nb_starting_cells_per_player) * (3/4))
    def moveCell(self, cell):
        posx = randint(0,self.parameters.map_width)
        posy = randint(0,self.parameters.map_height)
        self.calcule_hit([cell.position.x, cell.position.y], [posx, posy], cell.mass * self.parameters.radius_factor)
        self.actions.add_move_action(cell.pcell_id, posx, posy)
        
    def cutCell(self, cell):
        posx = randint(0,self.parameters.map_width)
        posy = randint(0,self.parameters.map_height)
        self.actions.add_divide_action(cell.pcell_id,posx,posy,int(cell.mass)//2)

    def createVirus(self, cell):
        posx = randint(0,self.parameters.map_width)
        posy = randint(0,self.parameters.map_height)
        self.actions.add_move_action(cell.pcell_id,posx,posy)

    def surrender(self):
        self.actions.add_surrender_action()

    def dist(self, x, y):
        f = x[0] - y[0]
        s = x[1] - y[1]
        return sqrt(f*f + s*s)
        
    def calcule_hit(self, x, y, radius):
        z = [y[0]] + [x[1]]
        alpha = asin(self.dist(y,z) / self.dist(x,y))
        z2 = [cos((90 - alpha) * 3.1415926 / 180) * radius, sin((90 - alpha) * 3.1415926 / 180) * radius]
        z3 = [cos((180 - (90 - alpha)) * 3.1415926 / 180) * radius, sin((180 - (90 - alpha)) * 3.1415926 / 180) * radius]
        z4 = [z2[0] + self.dist(x, y), z2[1] + self.dist(x, y)]
        z5 = [z3[0] + self.dist(x, y), z3[1] + self.dist(x, y)]
        print(str(z2) + " " + str(z3) + " " + str(z4) + " " + str(z5))        

        
    def playTurn(self, m_cells, e_cells, virus, neutral):
        self.actions.clear()
        sumMass = 0;
        for cell in m_cells:
            sumMass += cell.mass
        if(len(m_cells) > 0 and sumMass > self.baseMass):
            for cell in m_cells:
                res = randint(0,2)
                if(res == 0):
                    self.moveCell(cell)
                elif(res == 1 and len(m_cells) < self.parameters.max_cells_per_player):
                    self.cutCell(cell)
                else:
                    self.createVirus(cell)
        else:
            self.surrender()
                    

    def getAction(self):
        return self.actions
        
