from pyainl16 import *
import argparse
from iaAleatoire import *
from iaEuroTruckSimu import *
from iaAleatoireV31 import *
from random import randint



parser = argparse.ArgumentParser()
parser.add_argument('address', type=str)
parser.add_argument('port', type=int)
parser.add_argument('-name', type=str, default="EuroTruckSimu")
parser.add_argument('-type', type=int, default=2)
args = parser.parse_args()


class Connector:

    def connection (self, args):
        try:
            self.session = Session()
            self.session.connect(args.address, args.port)
            self.session.login_player(args.name)
            print("Logged")
        except AINetException as e:
            print("Exception : " + e.what())

    def wait_connect(self):
        try:
            self.welcome = self.session.wait_for_welcome()        
            self.params = self.welcome.parameters
            print("Welcome done")
        except AINetException as e:
            print("Exception : " + e.what())


    def wait_begin(self):
        try:
            self.session.wait_for_game_starts()
            print("Game Start !!")
        except AINetException as e:
            print("Exception : " + e.what())
        
    def is_logged(self):
        try:
            return self.session.is_logged()
        except AINetException as e:
            return False

    def wait_turn(self):
        try:
            self.session.wait_for_next_turn()
            self.m_cells = self.session.my_player_cells()
            self.e_cells = self.session.ennemy_player_cells()
            self.virus = self.session.viruses()
            self.neutral = self.session.neutral_cells()
        except AINetException as e:
            print("Exception : " + e.what())

    def send(self, actions):
        try:
            self.session.send_actions(actions)
        except AINetException as e:
            print("Exception : " + e.what())
            
        
def main(args):            

    connect = Connector()
    ia = None
    if(args.type == 0):
        ia = IaAleatoire()
    elif(args.type == 1):
        ia = iaEuroTruckSimu()
    else:
        ia = IaAleatoireV2()
        
    connect.connection(args)
    connect.wait_connect()
    
    ia.start(connect.welcome)
    connect.wait_begin()
    while connect.is_logged():
        connect.wait_turn()
        ia.playTurn(connect.m_cells, connect.e_cells, connect.virus, connect.neutral)
        connect.send(ia.getAction())
        
    print("Game finished")


main(args)
