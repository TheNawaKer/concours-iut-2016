from pyainl16 import *
from random import randint
from math import sqrt

class iaEuroTruckSimu:
    

    def start(self,gameParams):        
        self.actions = Actions()
        
    def moveCell(self, id):
        return
        
    def cutCell(self, id):
        return
        
    def createVirus(self, id):
        return
        
    def leaveCell(self, id):
        return
        
    def playTurn(self, m_cells, e_cells, virus, neutral):
        self.farme(m_cells[0], neutral)
        
    def getAction(self):
        return self.actions

    def farme(self, cell, neutral):
        if(len(neutral) > 1000): #On itere de facon aleatoire
            proche = -1
            proche_dist = -1
            for x in range(1000):
                z = randint(0, len(neutral) - 1)
                dist = sqrt((neutral[z].position.x - cell.position.x) * (neutral[z].position.x - cell.position.x)
                            + (neutral[z].position.y - cell.position.y) * (neutral[z].position.y - cell.position.y))
                if dist < proche_dist or proche_dist == -1:
                    proche_dist = dist
                    proche = z

            print("move" + str(neutral[proche].position))
            self.actions.add_move_action(cell.pcell_id, neutral[proche].position.x + 100,  neutral[proche].position.y + 100)
        else: #On itere sur toute
            proche = -1
            proche_dist = -1
            for x in neutral:
                dist = sqrt((neutral[x].position.x - cell.position.x) * (neutral[x].position.x - cell.position.x)
                            + (neutral[x].position.y - cell.position.y) * (neutral[x].position.y - cell.position.y))
                if dist < proche_dist or proche_dist == -1:
                    proche_dist = dist
                    proche = x
            self.actions.add_move_action(cell.pcell_id, neutral[proche].position.x + 100, neutral[proche].position.y)
